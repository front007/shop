/**
 * Created by Administrator on 2016/12/24.
 */
$('#fullpage').fullpage({
    sectionsColor: ['#0da5d6', '#2AB561', '#DE8910', '#16BA9D', '#0DA5D6'],
    afterLoad: function (a,k) {
        //console.log(k);
        $('.section').removeClass('current');
        setTimeout(function () {
            $('.section').eq( k - 1).addClass('current');
        },100);

    }
});